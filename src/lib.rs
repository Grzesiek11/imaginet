mod parser;
mod packet;
pub mod network;
pub mod client;
pub mod server;

const BYTE_BUFFER_SIZE: usize = 128;
const MPSC_BUFFER_SIZE: usize = 8;
