use std::{io, pin::Pin};

use futures_util::{Future, Stream};
use tokio::net::ToSocketAddrs;

use crate::parser;

pub trait Connection: Stream<Item = Result<Event, Error>> {
    fn connect<A, B>(addr: A, nickname: B) -> Pin<Box<dyn Future<Output = Result<Self, Error>> + 'static>> where A: ToSocketAddrs + 'static, B: Into<Vec<u8>> + 'static, Self: Sized;
    fn send<B>(&mut self, message: Message, recipients: Vec<B>) -> Pin<Box<dyn Future<Output = Result<(), Error>> + '_>> where B: Into<Vec<u8>>, B: 'static;
    fn user_list(&mut self) -> Pin<Box<dyn Future<Output = Result<Vec<Vec<u8>>, Error>> + '_>>;
    fn change_nickname<B>(&mut self, nickname: B) -> Pin<Box<dyn Future<Output = Result<(), Error>> + '_>> where B: Into<Vec<u8>>, B: 'static;
}

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    Parser(parser::Error),
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Self::Io(e)
    }
}

impl From<parser::Error> for Error {
    fn from(e: parser::Error) -> Self {
        Self::Parser(e)
    }
}

#[derive(Debug)]
pub enum Message {
    Word(Vec<u8>),
    Instructions(Vec<u8>),
    List(Vec<Vec<u8>>),
    Image(Vec<u8>),
    Object(Vec<u8>),
}

impl Message {
    pub fn word_from<B>(word: B) -> Self
        where B: Into<Vec<u8>>
    {
        Self::Word(word.into())
    }

    pub fn instructions_from<B>(instructions: B) -> Self
        where B: Into<Vec<u8>>
    {
        Self::Instructions(instructions.into())
    }

    pub fn list_from<B>(list: Vec<B>) -> Self
        where B: Into<Vec<u8>>
    {
        Self::List(list.into_iter().map(|w| w.into()).collect())
    }

    pub fn image_from<B>(bytes: B) -> Self
        where B: Into<Vec<u8>>
    {
        Self::Image(bytes.into())
    }

    pub fn object_from<B>(bytes: B) -> Self
        where B: Into<Vec<u8>>
    {
        Self::Object(bytes.into())
    }
}

#[derive(Debug)]
pub enum Event {
    RecievedMessage { sender: Vec<u8>, message: Message },
    SetNickname(Vec<u8>),
    ChangedNickname(Vec<u8>),
}
